package com.suyunyou.spider.core;

/**
 * 计数器
 * @author 岳静
 * @date 2016年6月24日 下午3:46:26 
 * @version V1.0
 */
public class Counter {

	//正常提取页面处理数
	private static int pageDownSuccNum = 0;
	//下载页面异常数
	private static int pageDownErrorNum = 0;

	//正常分析链接处理数
	private static int fetcherLinkSuccNum = 0;
	//正常分析链接异常数
	private static int fetcherLinkErrorNum = 0;
	
	public static void plusPageDownSuccNum() {
		pageDownSuccNum ++;
	}
	
	public static void plusPageDownErrorNum() {
		pageDownErrorNum ++;
	}
	
	public static void plusFetcherLinkSuccNum() {
		fetcherLinkSuccNum ++;
	}
	public static void plusFetcherLinkErrorNum() {
		fetcherLinkErrorNum ++;
	}

	public static int getPageDownSuccNum() {
		return pageDownSuccNum;
	}

	public static int getPageDownErrorNum() {
		return pageDownErrorNum;
	}

	public static int getFetcherLinkSuccNum() {
		return fetcherLinkSuccNum;
	}

	public static int getFetcherLinkErrorNum() {
		return fetcherLinkErrorNum;
	}
}