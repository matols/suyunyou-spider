package com.suyunyou.manager.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.manager.pojo.SysConfig;
import com.suyunyou.manager.service.SysConfigService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 系统配置的Controller
 * @author  yuejing
 * @email   yuejing0129@163.com 
 * @net		www.suyunyou.com
 * @date    2015年4月5日 下午10:21:22 
 * @version 1.0.0
 */
@Controller
public class SysConfigUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysConfigUiController.class);

	@Autowired
	private SysConfigService sysConfigService;
	
	@RequestMapping(value = "/sysConfig/f-view/manager")
	public String manger(HttpServletRequest request) {
		return "manager/sys/config-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sysConfig/f-json/pageQuery")
	@ResponseBody
	public ResponseFrame pageQuery(HttpServletRequest request, SysConfig sysConfig) {
		ResponseFrame frame = new ResponseFrame();
		Page<SysConfig> page = null;
		try {
			page = sysConfigService.pageQuery(sysConfig);
			frame.setBody(page);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/sysConfig/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, Integer id) {
		if(id != null) {
			modelMap.put("sysConfig", sysConfigService.get(id));
		}
		return "manager/sys/config-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/sysConfig/f-json/save")
	@ResponseBody
	public ResponseFrame save(HttpServletRequest request, SysConfig sysConfig) {
		ResponseFrame frame = new ResponseFrame();
		try {
			if(sysConfig.getId() == null) {
				sysConfigService.save(sysConfig);
			} else {
				sysConfigService.update(sysConfig);
			}
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
	
}