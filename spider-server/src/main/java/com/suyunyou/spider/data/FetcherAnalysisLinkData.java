package com.suyunyou.spider.data;

import java.util.ArrayList;
import java.util.List;

import com.system.comm.enums.Boolean;
import com.system.comm.utils.FrameTimeUtil;

/**
 * 分析子链接地址相关的数据
 * @author 岳静
 * @date 2016年6月24日 下午3:06:39 
 * @version V1.0
 */
public class FetcherAnalysisLinkData {
	/**
	 * 提取子链接的地址
	 */
	private static List<String> fetcherAnalysisLinks = new ArrayList<String>();

	/**
	 * 移除待提取子连接的链接
	 * @param link
	 */
	public static void remove(String link) {
		if(fetcherAnalysisLinks.contains(link)) {
			fetcherAnalysisLinks.remove(link);
		}
	}
	
	/**
	 * 获取待提取的第一个链接
	 * @return
	 */
	public static String get() {
		return fetcherAnalysisLinks.size() > 0 ? fetcherAnalysisLinks.get(0) : null; 
	}
	
	/**
	 * 设置为已提取内容链接
	 * @param link
	 * @param content
	 */
	public static void add(String link) {
		if(!fetcherAnalysisLinks.contains(link)) {
			//提取的信息不存在、并且待提取的也不存在才添加
			fetcherAnalysisLinks.add(link);
		}
	}

	/**
	 * 更新link的信息
	 * @param link
	 */
	public static void update(String link) {
		LinkData.updateFetcherAnalysisLink(link, Boolean.TRUE.getCode(), FrameTimeUtil.getTime());
		
		//移除待提取的子连接
		remove(link);
	}

	public static void clear() {
		fetcherAnalysisLinks.clear();
	}
	/*//key的前缀
	private final static String PREFIX = "fetcherAnalysisLinks";
	
	private static BaseCache cache;
	private static BaseCache getCache() {
		if(cache == null) {
			cache = new BaseCache();
		}
		return cache;
	}
	
	private static String keyFetcherAnalysisLinks() {
		return getCache().getKey(PREFIX);
	}
	
	*//**
	 * 移除待提取子连接的链接
	 * @param link
	 *//*
	public static synchronized void remove(String link) {
		String key = keyFetcherAnalysisLinks();
		List<String> list = getCache().get(key);
		if(list != null) {
			list.remove(link);
			getCache().set(key, list);
		}
	}
	
	*//**
	 * 获取待提取的第一个链接
	 * @return
	 *//*
	public static String get() {
		List<String> list = getCache().get(keyFetcherAnalysisLinks());
		return list != null && list.size() > 0 ? list.get(0) : null;
	}
	
	*//**
	 * 设置为已提取内容链接
	 * @param link
	 * @param content
	 *//*
	public static synchronized void add(String link) {
		String key = keyFetcherAnalysisLinks();
		List<String> list = getCache().get(key);
		if(list == null) {
			list = new ArrayList<String>();
		}
		if(!list.contains(link)) {
			//提取的信息不存在、并且待提取的也不存在才添加
			list.add(link);
			getCache().set(key, list);
		}
	}

	*//**
	 * 更新link的信息
	 * @param link
	 *//*
	public static synchronized void update(String link) {
		LinkData.updateFetcherAnalysisLink(link, Boolean.TRUE.getCode(), FrameTimeUtil.getTime());
		
		//移除待提取的子连接
		remove(link);
	}

	public static void reset() {
		String key = keyFetcherAnalysisLinks();
		getCache().set(key, null);
	}*/

}