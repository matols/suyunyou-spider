package com.suyunyou.spider.data;

import com.suyunyou.manager.service.SpiderLinkService;
import com.system.comm.utils.FrameSpringBeanUtil;

/**
 * 待提取的地址相关的数据
 * @author 岳静
 * @date 2016年6月24日 下午3:06:39 
 * @version V1.0
 */
public class FetcherLinkData {
	/**
	 * 提取的地址
	 */
	//private static List<String> fetcherLinks = new ArrayList<String>();

	private static SpiderLinkService getDb() {
		return FrameSpringBeanUtil.getBean(SpiderLinkService.class);
	}
	/**
	 * 添加提取的地址
	 * @param link
	 */
	public static void add(String link) {
		/*if(!fetcherLinks.contains(link)) {
			//提取的信息不存在、并且待提取的也不存在才添加
			fetcherLinks.add(link);
		}*/
		getDb().updateWaitFetcherContent(link);
	}
	
	
	/**
	 * 获取待提取的第一个链接
	 * @param downLinkErrorMaxNum 下载失败最大异常数
	 * @return
	 */
	public static String get(int downLinkErrorMaxNum) {
		return getDb().getWaitFetcherContent(downLinkErrorMaxNum);
		//return fetcherLinks.size() > 0 ? fetcherLinks.get(0) : null; 
	}
	
	/**
	 * 移除待提取的链接
	 * @param link
	 */
	/*public static void remove(String link) {
		fetcherLinks.remove(link);
	}

	public static void clear() {
		fetcherLinks.clear();
	}*/
	
	/*//key的前缀
	private final static String PREFIX = "fetcherLinks";
	
	private static BaseCache cache;
	private static BaseCache getCache() {
		if(cache == null) {
			cache = new BaseCache();
		}
		return cache;
	}
	
	private static String keyFetcherLinks() {
		return getCache().getKey(PREFIX);
	}
	
	*//**
	 * 添加提取的地址
	 * @param link
	 *//*
	public static synchronized void add(String link) {
		String key = keyFetcherLinks();
		List<String> list = getCache().get(key);
		if(list == null) {
			list = new ArrayList<String>();
		}
		if(!list.contains(link)) {
			//提取的信息不存在、并且待提取的也不存在才添加
			list.add(link);
			getCache().set(key, list);
		}
	}
	
	
	*//**
	 * 获取待提取的第一个链接
	 * @return
	 *//*
	public static String get() {
		List<String> list = getCache().get(keyFetcherLinks());
		return list != null && list.size() > 0 ? list.get(0) : null;
	}
	
	*//**
	 * 移除待提取的链接
	 * @param link
	 *//*
	public static synchronized void remove(String link) {
		String key = keyFetcherLinks();
		List<String> list = getCache().get(key);
		if(list != null) {
			list.remove(link);
			getCache().set(key, list);
		}
	}

	public static void reset() {
		String key = keyFetcherLinks();
		getCache().set(key, null);
	}*/
}