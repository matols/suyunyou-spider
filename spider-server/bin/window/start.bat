@echo off & setlocal enabledelayedexpansion

title spider-server

% 启动 %
echo Starting ...

java -Xms256m -Xmx256m -XX:MaxPermSize=64M -Dproject.dir=${user.dir}/../../ -jar ..\..\spider-server-1.0.0.war

:end
pause