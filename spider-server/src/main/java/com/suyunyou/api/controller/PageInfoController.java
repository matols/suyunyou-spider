package com.suyunyou.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.suyunyou.manager.pojo.PageInfo;
import com.suyunyou.manager.service.PageInfoService;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 微信号的Controller
 * @author yuejing
 * @date 2019-01-06 12:22:40
 * @version V1.0.0
 */
@RestController
public class PageInfoController extends BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PageInfoController.class);

	@Autowired
	private PageInfoService pageInfoService;

	@RequestMapping(name = "爬取的内容-分页查询信息", value = "/pageInfo/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
			@ApiParam(name="爬取时间", code="spiderTime", value="2019-01-07"),
			@ApiParam(name="来源网站编号", code="siteId", value="1"),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="pageId", pCode="rows", value=""),
			@ApiRes(name="标题", code="title", pCode="rows", value=""),
			@ApiRes(name="正文", code="content", pCode="rows", value=""),
			@ApiRes(name="爬取时间", code="spiderTime", pCode="rows", value=""),
			@ApiRes(name="作者", code="author", pCode="rows", value=""),
			@ApiRes(name="爬取地址", code="spiderUrl", pCode="rows", value=""),
			@ApiRes(name="来源域名编号", code="siteId", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(PageInfo pageInfo, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				pageInfo.setOrderbys(orderbys);
			}
			ResponseFrame frame = pageInfoService.pageQuery(pageInfo);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

}