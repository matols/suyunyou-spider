<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<div class="c-left left-menu">
	<div class="panel-group">
		<div class="panel panel-info top">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a href="${webroot}/sysUser/f-view/main">个人中心</a>
				</h4>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'spider'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapse2">提取任务 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapse2" class="panel-collapse collapse <c:if test="${param.first == 'spider'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'siteMgr'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/spiderSite/f-view/manager">提取网站</a>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'linkMgr'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/spiderLink/f-view/manager">链接列表</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'page'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapsePage">内容管理 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapsePage" class="panel-collapse collapse <c:if test="${param.first == 'page'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'pageInfoMgr'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/pageInfo/f-view/manager">内容列表</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default bottom">
			<div class="panel-heading <c:if test="${param.first == 'sys'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapse1">系统管理 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapse1" class="panel-collapse collapse <c:if test="${param.first == 'sys'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'userManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysUser/f-view/manager">用户管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'configManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysConfig/f-view/manager">系统配置</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>