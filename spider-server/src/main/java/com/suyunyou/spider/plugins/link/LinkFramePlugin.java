package com.suyunyou.spider.plugins.link;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.suyunyou.spider.plugins.ILinkPlugin;

/**
 * 提取Frame的链接
 * @author yuejing
 * @date 2018年1月26日 下午2:42:20
 */
public class LinkFramePlugin extends ILinkPlugin {

	@Override
	public List<String> getLinks(String content) {
		List<String> list = new ArrayList<String>();
		Pattern pattern = Pattern.compile("(?i)(?s)<\\s*?frame.*?src=\"(.*?)\".*?>");
		Matcher matcher = pattern.matcher(content);

		while (matcher.find()) {
			list.add(matcher.group(1));
		}
		return list;
	}

}