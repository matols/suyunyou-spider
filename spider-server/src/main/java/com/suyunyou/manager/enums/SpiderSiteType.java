package com.suyunyou.manager.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.system.comm.model.KvEntity;

/**
 * 类型[10普通网站、20公众号]
 * @author yuejing
 * @date 2014年11月16日 下午7:52:03
 * @version V1.0.0
 */
public enum SpiderSiteType {
	WEBSITE		(10, "普通网站"),
	WEIXIN_GZH	(20, "公众号");
	
	public static final String KEY = "spider_site_type";
	
	private int code;
	private String name;
	private static List<KvEntity> list = new ArrayList<KvEntity>();
	private static Map<Integer, String> map = new HashMap<Integer, String>();

	private SpiderSiteType(Integer code, String name) {
		this.code = code;
		this.name = name;
	}
	
	static {
		Set<SpiderSiteType> set = EnumSet.allOf(SpiderSiteType.class);
		for(SpiderSiteType e : set){
			map.put(e.getCode(), e.getName());
			list.add(new KvEntity(Integer.toString(e.getCode()), e.getName()));
		}
	}

	/**
	 * 根据Code获取对应的汉字
	 * @param code
	 * @return
	 */
	public static String getText(Integer code) {
		return map.get(code);
	}
	
	/**
	 * 获取集合
	 * @return
	 */
	public static List<KvEntity> getList() {
		return list;
	}

	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}
