package com.suyunyou.spider.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 链接的实体
 * @author 岳静
 * @date 2016年6月27日 上午10:55:20 
 * @version V1.0
 */
public class Link implements Serializable {
	private static final long serialVersionUID = 2960060268872752987L;

	//链接
	private String link;
	//来源域名
	private String domain;
	//内容
	private String content;
	//创建时间
	private Date createTime;
	//提取内容时间
	private Date fetcherContentTime;
	//是否提取链接
	private Integer isFetcherLink;
	//提取链接时间
	private Date fetcherLinkTime;
	//来源网址编号
	private Integer siteId;
	//来源网址入口地址
	private String siteUrl;
	//下载失败次数
	private Integer downErrorNum;
	
	public Link() {
		super();
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public Date getFetcherContentTime() {
		return fetcherContentTime;
	}
	public void setFetcherContentTime(Date fetcherContentTime) {
		this.fetcherContentTime = fetcherContentTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getIsFetcherLink() {
		return isFetcherLink;
	}
	public void setIsFetcherLink(Integer isFetcherLink) {
		this.isFetcherLink = isFetcherLink;
	}
	public Date getFetcherLinkTime() {
		return fetcherLinkTime;
	}
	public void setFetcherLinkTime(Date fetcherLinkTime) {
		this.fetcherLinkTime = fetcherLinkTime;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public Integer getDownErrorNum() {
		return downErrorNum;
	}
	public void setDownErrorNum(Integer downErrorNum) {
		this.downErrorNum = downErrorNum;
	}
	public String getSiteUrl() {
		return siteUrl;
	}
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
}