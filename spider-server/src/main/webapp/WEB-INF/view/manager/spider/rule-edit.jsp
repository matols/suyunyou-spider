<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑提取规则</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="ruleId" value="${spiderRule.ruleId}">
		<input type="hidden" id="siteId" value="${param.siteId}">
  		<div class="form-group">
			<input type="text" class="form-control" id="regex" placeholder="提取内容规则(为提取文章详情地址的正则表达式)" value="${spiderRule.regex}">
		</div>
  		<div class="form-group">
			<input type="text" class="form-control" id="titleSelect" placeholder="标题选择器(Jsoup的样式选择器，语法和JQuery一致)" value="${spiderRule.titleSelect}">
		</div>
  		<div class="form-group">
			<input type="text" class="form-control" id="contentSelect" placeholder="内容选择器(Jsoup的样式选择器，语法和JQuery一致)" value="${spiderRule.contentSelect}">
		</div>
		<div class="form-group">执行状态：&nbsp; &nbsp; <my:radio id="isEnable" name="isEnable" dictcode="boolean" value="${spiderRule.isEnable}" defvalue="1" /></div>
		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			var _regex = $('#regex');
			if(JUtil.isEmpty(_regex.val())) {
				_saveMsg.append('请输入内容提取规则');
				_regex.focus();
				return;
			}
			var _titleSelect = $('#titleSelect');
			if(JUtil.isEmpty(_titleSelect.val())) {
				_saveMsg.append('请输入标题选择器');
				_titleSelect.focus();
				return;
			}
			var _contentSelect = $('#contentSelect');
			if(JUtil.isEmpty(_contentSelect.val())) {
				_saveMsg.append('请输入内容提取规则');
				_contentSelect.focus();
				return;
			}

			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			var _isEnable = $('input[name="isEnable"]:checked').val();
			JUtil.ajax({
				url : '${webroot}/spiderRule/f-json/save',
				data : {
					ruleId: $('#ruleId').val(),
					siteId: $('#siteId').val(),
					regex: _regex.val(),
					titleSelect: _titleSelect.val(),
					contentSelect: _contentSelect.val(),
					isEnable: _isEnable
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>