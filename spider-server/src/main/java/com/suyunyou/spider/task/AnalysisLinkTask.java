package com.suyunyou.spider.task;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.spider.core.Counter;
import com.suyunyou.spider.data.FetcherAnalysisLinkData;
import com.suyunyou.spider.utils.AnalysisLinkUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.threadpool.FrameThreadAction;

/**
 * 分析可以提取的链接
 * @author yuejing
 * @date 2018年1月25日 下午1:27:45
 */
public class AnalysisLinkTask extends FrameThreadAction {

	private static final long serialVersionUID = -4514045785524480607L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisLinkTask.class);

	public AnalysisLinkTask() {
	}

	@Override
	protected void compute() {
		try {
			String link = FetcherAnalysisLinkData.get();
			if(FrameStringUtil.isEmpty(link)) {
				return;
			}
			AnalysisLinkUtil.fetcher(link);
			Counter.plusFetcherLinkSuccNum();
			Thread.sleep(500);
		} catch (Exception e) {
			LOGGER.error("分析链接异常: " + e.getMessage(), e);
			Counter.plusFetcherLinkErrorNum();
		}
	}
}