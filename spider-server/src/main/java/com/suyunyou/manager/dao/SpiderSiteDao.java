package com.suyunyou.manager.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.suyunyou.manager.pojo.SpiderSite;

public interface SpiderSiteDao {

	public abstract void save(SpiderSite spiderSite);

	public abstract void update(SpiderSite spiderSite);

	public abstract SpiderSite get(@Param("siteId")Integer siteId);

	public abstract List<SpiderSite> findAll();

	public abstract void delete(@Param("siteId")Integer siteId);

	public abstract List<SpiderSite> findEnable();

	public abstract void updateRuleTime(@Param("siteId")Integer siteId, @Param("ruleTime")Date ruleTime);

}