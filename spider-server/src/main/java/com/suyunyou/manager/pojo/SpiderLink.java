package com.suyunyou.manager.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * 提取链接实体
 * @author yuejing
 * @date 2015年4月5日 下午10:09:28
 * @version V1.0.0
 */
@Alias("spiderLink")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class SpiderLink extends BaseEntity implements Serializable {
	//编号
	private Integer linkId;
	//链接
	private String link;
	//md5_link
	private String md5Link;
	//来源域名
	private String domain;
	//内容
	private String content;
	//创建时间
	private Date createTime;
	//是否提取内容
	private Integer isFetcherContent;
	//提取内容时间
	private Date fetcherContentTime;
	//是否提取链接
	private Integer isFetcherLink;
	//提取链接时间
	private Date fetcherLinkTime;
	//来源网址编号
	private Integer siteId;
	//下载失败次数
	private Integer downErrorNum;
	
	//========================== 扩展属性
	// 站点信息
	private SpiderSite site;
	
	public Integer getLinkId() {
		return linkId;
	}
	public void setLinkId(Integer linkId) {
		this.linkId = linkId;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getFetcherContentTime() {
		return fetcherContentTime;
	}
	public void setFetcherContentTime(Date fetcherContentTime) {
		this.fetcherContentTime = fetcherContentTime;
	}
	public Integer getIsFetcherLink() {
		return isFetcherLink;
	}
	public void setIsFetcherLink(Integer isFetcherLink) {
		this.isFetcherLink = isFetcherLink;
	}
	public Date getFetcherLinkTime() {
		return fetcherLinkTime;
	}
	public void setFetcherLinkTime(Date fetcherLinkTime) {
		this.fetcherLinkTime = fetcherLinkTime;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getMd5Link() {
		return md5Link;
	}
	public void setMd5Link(String md5Link) {
		this.md5Link = md5Link;
	}
	public Integer getDownErrorNum() {
		return downErrorNum;
	}
	public void setDownErrorNum(Integer downErrorNum) {
		this.downErrorNum = downErrorNum;
	}
	public Integer getIsFetcherContent() {
		return isFetcherContent;
	}
	public void setIsFetcherContent(Integer isFetcherContent) {
		this.isFetcherContent = isFetcherContent;
	}
	public SpiderSite getSite() {
		return site;
	}
	public void setSite(SpiderSite site) {
		this.site = site;
	}
}