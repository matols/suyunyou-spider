package com.suyunyou.manager.pojo;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * 提取规则实体
 * @author yuejing
 * @date 2015年4月5日 下午10:09:28
 * @version V1.0.0
 */
@Alias("spiderRule")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class SpiderRule extends BaseEntity implements Serializable {
	//编号
	private Integer ruleId;
	//网站编号
	private Integer siteId;
	//页面表达式
	private String regex;
	//标题选择器
	private String titleSelect;
	//内容选择器
	private String contentSelect;
	//是否启用[0否、1是]
	private Integer isEnable;
	
	public SpiderRule() {
		super();
	}

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getTitleSelect() {
		return titleSelect;
	}

	public void setTitleSelect(String titleSelect) {
		this.titleSelect = titleSelect;
	}

	public String getContentSelect() {
		return contentSelect;
	}

	public void setContentSelect(String contentSelect) {
		this.contentSelect = contentSelect;
	}

	public Integer getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}
}