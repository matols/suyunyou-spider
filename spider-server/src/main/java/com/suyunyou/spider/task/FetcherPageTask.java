package com.suyunyou.spider.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.spider.data.FetcherPageLinkData;
import com.suyunyou.spider.data.LinkData;
import com.suyunyou.spider.plugins.IPagePlugin;
import com.suyunyou.spider.utils.PluginUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.threadpool.FrameThreadAction;

/**
 * 提取正文
 * @author yuejing
 * @date 2018年1月25日 下午1:27:34
 */
public class FetcherPageTask extends FrameThreadAction {

	private static final long serialVersionUID = -8878461700070258924L;
	private static final Logger LOGGER = LoggerFactory.getLogger(FetcherPageTask.class);

	public FetcherPageTask() {
	}

	@Override
	protected void compute() {
		String link = FetcherPageLinkData.get();
		try {
			if(FrameStringUtil.isEmpty(link)) {
				return;
			}
			boolean isSucc = false;
			//需要扫描指定路劲下的类
			List<IPagePlugin> plugins = PluginUtil.getPagePlugins();
			for (IPagePlugin plugin : plugins) {
				try {
					plugin.setLink(LinkData.get(link));
					if(plugin.handlePageDtl()) {
						FetcherPageLinkData.remove(link);
						isSucc = true;
					}
				} catch (Exception e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
			if(!isSucc) {
				//全部插件都不通过，移除链接
				FetcherPageLinkData.remove(link);
			}
		} catch (Exception e) {
			LOGGER.error("链接[" + link + "]提取正文异常: " + e.getMessage());
		}
	}
}