package com.suyunyou.manager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SpiderRuleDao;
import com.suyunyou.manager.pojo.SpiderRule;

/**
 * 提取网站的Service
 * @author yuejing
 * @date 2016年6月26日 上午8:31:45
 * @version V1.0.0
 */
@Component
public class SpiderRuleService {

	@Autowired
	private SpiderRuleDao spiderRuleDao;
	
	/**
	 * 保存
	 * @param spiderSite
	 */
	public void saveOrUpdate(SpiderRule spiderRule) {
		if(spiderRule.getRuleId() == null) {
			spiderRuleDao.save(spiderRule);
		} else {
			spiderRuleDao.update(spiderRule);
		}
	}

	/**
	 * 根据编号获取对象
	 * @param ruleId
	 * @return
	 */
	public SpiderRule get(Integer ruleId) {
		return spiderRuleDao.get(ruleId);
	}

	/**
	 * 根据网站编号获取所有
	 * @return
	 */
	public List<SpiderRule> findBySiteId(Integer siteId) {
		return spiderRuleDao.findBySiteId(siteId);
	}

	/**
	 * 删除
	 * @param ruleId
	 */
	public void delete(Integer ruleId) {
		spiderRuleDao.delete(ruleId);
	}

	/**
	 * 获取所有启用的规则
	 * @return
	 */
	public List<SpiderRule> findEnable() {
		return spiderRuleDao.findEnable();
	}

}