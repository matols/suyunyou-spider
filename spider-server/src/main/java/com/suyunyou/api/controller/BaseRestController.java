package com.suyunyou.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.system.comm.utils.FrameTimeUtil;

public class BaseRestController {
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(FrameTimeUtil.FMT_YYYY_MM_DD);
			dateFormat.setLenient(false);
			//true:允许输入空值，false:不能为空值
			binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		}catch(Exception e) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(FrameTimeUtil.FMT_DEFAULT);
			dateFormat.setLenient(false);
			//true:允许输入空值，false:不能为空值
			binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		}
	}
}
