<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-链接列表</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/manager/comm/left.jsp">
			<jsp:param name="first" value="spider"/>
			<jsp:param name="second" value="linkMgr"/>
		</jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">提取任务 / <b>链接列表</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-6 enter-panel">
								<div class="btn-group">
									<my:select id="siteId" items="${sites}" cssCls="form-control input-sm w-120" value="" exp="onchange=\"info.loadInfo(1)\""/>
								</div>
								<div class="btn-group">
									<my:select id="isFetcherContent" dictcode="boolean" cssCls="form-control input-sm w-120" headerKey="" headerValue="是否提取内容" value="" exp="onchange=\"info.loadInfo(1)\""/>
								</div>
								<div class="btn-group">
									<my:select id="isFetcherLink" dictcode="boolean" cssCls="form-control input-sm w-120" headerKey="" headerValue="是否分析链接 " value="" exp="onchange=\"info.loadInfo(1)\""/>
								</div>
								<!-- <input type="text" style="width: 150px;display: inline;" class="form-control input-sm" id="receUserId" placeholder="接收人编码[精确查找]" value=""> -->
							  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
							</div>
							<div class="col-sm-6 text-right">
								<small class="text-muted">根据左边条件操作</small>
							  	<div class="btn-group">
							  		<a href="javascript:info.batchDel()" class="btn btn-sm btn-danger btn-sm">批量删除</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>链接</th>',
				                         '<th>创建时间</th>',
				                         '<th>内容爬取时间</th>',
				                         '<th>链接分析时间</th>',
				                         '<th width="100">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;

			JUtil.ajax({
				url : webroot + '/spiderLink/f-json/pageQuery',
				data : { page:infoPage.page, size:infoPage.size, siteId: $('#siteId').val(),
					isFetcherContent: $('#isFetcherContent').val(), isFetcherLink: $('#isFetcherLink').val() },
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							return ['<tr>',
							    	'<td><a href="',obj.link,'" target="_blank" class="text-success" title="',obj.link,'">',obj.link.length>80?obj.link.substring(0, 80):obj.link,'</a></td>',
							    	'<td>',obj.createTime,'</td>',
							    	'<td>',obj.isFetcherContent===0?'<span class="label label-warning">待爬取</span>':obj.fetcherContentTime,'</td>',
							    	'<td>',obj.isFetcherLink===0?'<span class="label label-warning">待分析</span>':obj.fetcherLinkTime,'</td>',
							    	'<td>',
							    	'<a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(',obj.linkId,')" title="删除"></a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		del : function(id) {
			if(confirm('您确定要删除该记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/spiderLink/f-json/delete',
					data : { linkId: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		batchDel: function() {
			if(confirm('您确定要批量删除记录吗?')) {
				JUtil.ajax({
					url : '${webroot}/spiderLink/f-json/deleteBatch',
					data : {
						siteId: $('#siteId').val(),
						isFetcherContent: $('#isFetcherContent').val(),
						isFetcherLink: $('#isFetcherLink').val()
					},
					success : function(json) {
						if (json.code === 0) {
							message('批量删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo(1);
});
</script>
</body>
</html>