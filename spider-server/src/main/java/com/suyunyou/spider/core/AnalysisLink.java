package com.suyunyou.spider.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.manager.enums.Config;
import com.suyunyou.manager.service.SysConfigService;
import com.suyunyou.spider.task.AnalysisLinkTask;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.threadpool.FrameThreadPool;
import com.system.threadpool.FrameThreadPoolUtil;

/**
 * 分析链接
 * @author 岳静
 * @date 2016年6月24日 下午2:05:18 
 * @version V1.0
 */
public class AnalysisLink {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisLink.class);
	private boolean run = true;
	private FrameThreadPool threadPool;
	private SysConfigService configService;
	public AnalysisLink() {
		threadPool = FrameThreadPoolUtil.getThreadPool("analysis-link");
		configService = FrameSpringBeanUtil.getBean(SysConfigService.class);
	}
	
	/**
	 * 开启爬取任务
	 */
	public void start() {
		LOGGER.info("进入分析页面链接的任务");
		final int sleep = Integer.valueOf(configService.getValue(Config.SLEEP_ANALYSIS_LINK, "200"));
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (run) {
					threadPool.execute(new AnalysisLinkTask());
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
						LOGGER.error("休眠异常");
					}
				}
			}
		}).start();
	}
	
	/**
	 * 停止
	 */
	public void stop() {
		run = false;
		threadPool.destroy();
	}
}
