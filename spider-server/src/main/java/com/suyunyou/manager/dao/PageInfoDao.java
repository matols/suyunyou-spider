package com.suyunyou.manager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.suyunyou.manager.pojo.PageInfo;

public interface PageInfoDao {

	public abstract void save(PageInfo pageInfo);

	public abstract PageInfo getSpiderUrl(@Param("spiderUrl")String spiderUrl);

	public abstract void delete(@Param("pageId")Integer pageId);

	public abstract List<PageInfo> findPageInfo(PageInfo pageInfo);
	public abstract int findPageInfoCount(PageInfo pageInfo);

	public abstract PageInfo getDtl(@Param("pageId")Integer pageId);

}