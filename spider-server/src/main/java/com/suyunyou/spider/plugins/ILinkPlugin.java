package com.suyunyou.spider.plugins;

import java.util.List;

/**
 * 链接规则的插件接口
 * @author yuejing
 * @date 2016年6月25日 下午4:38:39
 * @version V1.0.0
 */
public abstract class ILinkPlugin {
	
	public abstract List<String> getLinks(String content);
}