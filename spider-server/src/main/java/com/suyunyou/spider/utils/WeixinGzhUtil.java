package com.suyunyou.spider.utils;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.manager.pojo.SpiderSite;
import com.suyunyou.spider.plugins.link.LinkDefaultPlugin;

public class WeixinGzhUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeixinGzhUtil.class);

	/**
	 * 获取公众号的最新的链接
	 * @param site
	 * @return
	 */
	public static String gzhLink(SpiderSite site) {
		try {
			String content = PageDownUtil.get(site.getUrl());
			//获取和公众号名称一致的公众号的链接
			return getLinks(content, site.getName());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

	private static String getLinks(String content, String name) {
		Document doc = Jsoup.parse(content);
		if (doc == null) {
			return null;
		}
		Element title = doc.select(".txt-box>.tit").first();
		if (title == null) {
			return null;
		}
		String linkContent = title.html();
		LinkDefaultPlugin plugin = new LinkDefaultPlugin();
		List<String> links = plugin.getLinks(linkContent);
		if(links != null) {
			for (String link : links) {
				return link.replace("&amp;", "&");
			}
		}
		return null;
	}

	public static void main(String[] args) {
		SpiderSite site = new SpiderSite();
		site.setSiteId(1);
		site.setType(20);
		site.setName("巨国贤");
		site.setUrl("http://weixin.sogou.com/weixin?type=1&s_from=input&query=%E5%B7%A8%E5%9B%BD%E8%B4%A4");
		site.setDomain("https://mp.weixin.qq.com/s?");
		String url = gzhLink(site);
		System.out.println(url);
	}
}
