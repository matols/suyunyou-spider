package com.suyunyou.manager.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SpiderLinkDao;
import com.suyunyou.manager.pojo.SpiderLink;
import com.system.cache.redis.BaseCache;

@Component
public class SpiderLinkCache extends BaseCache {

	@Autowired
	private SpiderLinkDao spiderLinkDao;

	private String getLinkKey(String link) {
		return "link_" + link;
	}
	public SpiderLink getLink(String link) {
		String key = getLinkKey(link);
		SpiderLink sl = super.get(key);
		if (sl == null) {
			sl = spiderLinkDao.getLink(link);
			super.set(key, sl);
		}
		return sl;
	}
	public void del(String link) {
		String key = getLinkKey(link);
		super.delete(key);
	}


	
}
