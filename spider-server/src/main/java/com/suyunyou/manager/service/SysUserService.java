package com.suyunyou.manager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SysUserDao;
import com.suyunyou.manager.enums.GeneralStatus;
import com.suyunyou.manager.pojo.SysUser;
import com.suyunyou.manager.utils.SysUserUtil;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;

/**
 * sys_user的Service
 * @author yuejing
 * @date 2015-03-30 14:07:27
 * @version V1.0.0
 */
@Component
public class SysUserService {

	@Autowired
	private SysUserDao sysUserDao;

	/**
	 * 登录
	 * @param usrInfo
	 * @return
	 */
	public ResponseFrame login(SysUser sysUser) {
		ResponseFrame frame = new ResponseFrame();
		SysUser user = getByUsername(sysUser.getUsername());
		if(user == null) {
			//不存在用户
			frame.setCode(-2);
			frame.setMessage("请输入正确的用户和密码!");
			return frame;
		}
		if(user.getStatus().intValue() == GeneralStatus.FREEZE.getCode().intValue()) {
			//帐号冻结
			frame.setCode(-3);
			frame.setMessage("您的帐号被冻结, 快去联系客服吧!");
			return frame;
		}
		if(!SysUserUtil.encodePassword(sysUser.getPassword()).equalsIgnoreCase(user.getPassword())) {
			//密码不正确
			frame.setCode(-4);
			frame.setMessage("请输入正确的用户和密码!");
			return frame;
		}
		//密码相同
		frame.setBody(user);
		frame.setSucc();
		return frame;
	}

	private SysUser getByUsername(String username) {
		return sysUserDao.getByUsername(username);
	}

	/**
	 * 保存
	 * @param sysUser
	 */
	public void save(SysUser sysUser) {
		if (FrameStringUtil.isNotEmpty(sysUser.getPassword())) {
			String password = SysUserUtil.encodePassword(sysUser.getPassword());
			sysUser.setPassword(password);
		}
		sysUserDao.save(sysUser);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void delete(Integer id) {
		sysUserDao.delete(id);
	}

	/**
	 * 修改
	 * @param sysUser
	 */
	public void update(SysUser sysUser) {
		if(FrameStringUtil.isNotEmpty(sysUser.getPassword())) {
			String password = SysUserUtil.encodePassword(sysUser.getPassword());
			sysUser.setPassword(password);
		}
		sysUserDao.update(sysUser);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public SysUser get(Integer id) {
		return sysUserDao.get(id);
	}

	/**
	 * 分页获取对象
	 * @param sysUser
	 * @return
	 */
	public Page<SysUser> pageQuery(SysUser sysUser) {
		sysUser.setDefPageSize();
		int total = sysUserDao.findSysUserCount(sysUser);
		List<SysUser> rows = null;
		if(total > 0) {
			rows = sysUserDao.findSysUser(sysUser);
			for (SysUser user : rows) {
				user.setStatusname(GeneralStatus.getText(user.getStatus()));
			}
		}
		Page<SysUser> page = new Page<SysUser>(sysUser.getPage(), sysUser.getSize(), total, rows);
		return page;
	}

}