package com.suyunyou.manager.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.manager.pojo.SpiderRule;
import com.suyunyou.manager.service.SpiderRuleService;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 提取规则的Controller
 * @author yuejing
 * @date 2016年6月26日 下午3:28:20
 * @version V1.0.0
 */
@Controller
public class SpiderRuleUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderRuleUiController.class);

	@Autowired
	private SpiderRuleService spiderRuleService;

	@RequestMapping(value = "/spiderRule/f-view/manager")
	public String manager(HttpServletRequest request, ModelMap modelMap) {
		return "manager/spider/rule-manager";
	}
	
	/**
	 * 获取信息
	 * @return
	 */
	@RequestMapping(value = "/spiderRule/f-json/findBySiteId")
	@ResponseBody
	public ResponseFrame findAll(HttpServletRequest request, Integer siteId) {
		ResponseFrame frame = new ResponseFrame();
		List<SpiderRule> list = null;
		try {
			list = spiderRuleService.findBySiteId(siteId);
			frame.setBody(list);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("获取信息异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/spiderRule/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, Integer id) {
		if(id != null) {
			modelMap.put("spiderRule", spiderRuleService.get(id));
		}
		return "manager/spider/rule-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/spiderRule/f-json/save")
	@ResponseBody
	public ResponseFrame save(HttpServletRequest request, SpiderRule spiderRule) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderRuleService.saveOrUpdate(spiderRule);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/spiderRule/f-json/delete")
	@ResponseBody
	public ResponseFrame delete(HttpServletRequest request, Integer id) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderRuleService.delete(id);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
}
