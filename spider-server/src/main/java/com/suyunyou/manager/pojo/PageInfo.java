package com.suyunyou.manager.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * 服务均衡实体
 * @author yuejing
 * @date 2015年4月5日 下午10:09:28
 * @version V1.0.0
 */
@Alias("pageInfo")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PageInfo extends BaseEntity implements Serializable {
	//编号
	private Integer pageId;
	//标题
	private String title;
	//正文
	private String content;
	//完整html
	private String html;
	//爬取时间
	private Date spiderTime;
	//作者
	private String author;
	//爬取地址
	private String spiderUrl;
	//来源域名编号
	private Integer siteId;
	
	//============================== 扩展属性
	// 开始时间
	private Date beginTime;
	// 结束时间
	private Date endTime;
	
	public PageInfo() {
		super();
	}
	public PageInfo(Integer siteId, String title, String content, String html,
			String author, String spiderUrl) {
		super();
		this.siteId = siteId;
		this.title = title;
		this.content = content;
		this.html = html;
		this.author = author;
		this.spiderUrl = spiderUrl;
	}
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public Date getSpiderTime() {
		return spiderTime;
	}
	public void setSpiderTime(Date spiderTime) {
		this.spiderTime = spiderTime;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSpiderUrl() {
		return spiderUrl;
	}
	public void setSpiderUrl(String spiderUrl) {
		this.spiderUrl = spiderUrl;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}