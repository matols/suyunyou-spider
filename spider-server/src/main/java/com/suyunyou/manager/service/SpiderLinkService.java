package com.suyunyou.manager.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SpiderLinkDao;
import com.suyunyou.manager.pojo.SpiderLink;
import com.system.comm.enums.Boolean;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameMd5Util;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 提取链接的Service
 * @author yuejing
 * @date 2016年6月26日 上午8:31:45
 * @version V1.0.0
 */
@Component
public class SpiderLinkService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderLinkService.class);
	@Autowired
	private SpiderLinkDao spiderLinkDao;
	@Autowired
	private SpiderSiteService spiderSiteService;
	/*@Autowired
	private SpiderLinkCache spiderLinkCache;*/
	
	/**
	 * 新增
	 * @param link
	 * @param domain
	 * @param siteId
	 */
	public synchronized void save(String link, String domain, Integer siteId) {
		SpiderLink sl = getLink(link);
		if(sl == null) {
			sl = new SpiderLink();
			sl.setLink(link);
			sl.setMd5Link(getMd5Link(link));
			sl.setDomain(domain);
			sl.setSiteId(siteId);
			sl.setIsFetcherContent(Boolean.FALSE.getCode());
			sl.setIsFetcherLink(Boolean.FALSE.getCode());
			try {
				spiderLinkDao.save(sl);
			} catch (Exception e) {
				LOGGER.error("保存页面出现异常: " + e.getMessage());
			}
		}
	}

	private String getMd5Link(String link) {
		return FrameMd5Util.getInstance().encodePassword(link);
	}

	/**
	 * 根据爬取地址获取对象
	 * @param link
	 * @return
	 */
	public SpiderLink getLink(String link) {
		SpiderLink sl = spiderLinkDao.getLink(getMd5Link(link));
		if (sl != null) {
			sl.setSite(spiderSiteService.get(sl.getSiteId()));
		}
		return sl;
	}

	/**
	 * 根据链接修改内容
	 * @param link
	 * @param content
	 */
	public void updateContent(String link, String content) {
		link = getMd5Link(link);
		spiderLinkDao.updateContent(link, content, Boolean.TRUE.getCode());
		//spiderLinkCache.del(link);
	}

	/**
	 * 根据链接修改是否提取
	 * @param link
	 * @param isFetcherLink
	 */
	public void updateFetcherLink(String link, Integer isFetcherLink) {
		link = getMd5Link(link);
		spiderLinkDao.updateFetcherLink(link, isFetcherLink);
		//spiderLinkCache.del(link);
	}

	/**
	 * 修改下载失败次数+1
	 * @param link
	 * @return
	 */
	public int updateDownErrorNum(String link) {
		String md5Link = getMd5Link(link);
		spiderLinkDao.updateDownErrorNum(md5Link);
		//spiderLinkCache.del(md5Link);
		Integer num = spiderLinkDao.getDownErrorNum(md5Link);
		return num == null ? 0 : num;
	}

	/**
	 * 获取待提取内容的链接
	 * @param downLinkErrorMaxNum 
	 * @return
	 */
	public String getWaitFetcherContent(int downLinkErrorMaxNum) {
		//return spiderLinkDao.getWaitFetcherContent(Constant.serviceStartTime, downLinkErrorMaxNum);
		return spiderLinkDao.getWaitFetcherContent(downLinkErrorMaxNum);
	}

	/**
	 * 将该链接修改为待提取内容
	 * @param link
	 */
	public void updateWaitFetcherContent(String link) {
		link = getMd5Link(link);
		spiderLinkDao.updateWaitFetcherContent(link);
		//spiderLinkCache.del(link);
	}

	/**
	 * 修改提取状态为成功
	 * @param link
	 */
	public void updateDbSuccIsFetcherContent(String link) {
		link = getMd5Link(link);
		spiderLinkDao.updateDbSuccIsFetcherContent(link);
		//spiderLinkCache.del(link);
	}

	public ResponseFrame pageQuery(SpiderLink spiderLink) {
		spiderLink.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = spiderLinkDao.findSpiderLinkCount(spiderLink);
		List<SpiderLink> data = null;
		if(total > 0) {
			data = spiderLinkDao.findSpiderLink(spiderLink);
		}
		Page<SpiderLink> page = new Page<SpiderLink>(spiderLink.getPage(), spiderLink.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	public void delete(Integer linkId) {
		spiderLinkDao.delete(linkId);
	}

	public void deleteBatch(Integer siteId, Integer isFetcherContent,
			Integer isFetcherLink) {
		spiderLinkDao.deleteBatch(siteId, isFetcherContent, isFetcherLink);
	}

	public void updateDomainBySiteId(Integer siteId, String domain) {
		spiderLinkDao.updateDomainBySiteId(siteId, domain);
	}
}